package UNI.FIIS.POO.FIIS;

public class Paquete {
    public String codigo;
    public Hotel hotel;
    public Vuelo vuelo;
    public Tour tour;

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }

    public Vuelo getVuelo() {
        return vuelo;
    }

    public void setVuelo(Vuelo vuelo) {
        this.vuelo = vuelo;
    }

    public Tour getTour() {
        return tour;
    }

    public void setTour(Tour tour) {
        this.tour = tour;
    }

    public Paquete(String codigo, Hotel hotel, Vuelo vuelo, Tour tour) {
        this.codigo = codigo;
        this.hotel = hotel;
        this.vuelo = vuelo;
        this.tour = tour;
    }
}
