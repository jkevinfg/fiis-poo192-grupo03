package UNI.FIIS.POO.FIIS;

import java.util.Date;

public class Vuelo {
    private int nrodeVuelo;
    private String origen;
    private String destino;
    private String tipodevuelo;
    private Date fechasalida;
    private Date fecharegreso;

    public Vuelo(int nrodeVuelo, String origen, String destino, String tipodevuelo, Date fechasalida, Date fecharegreso) {
        this.nrodeVuelo = nrodeVuelo;
        this.origen = origen;
        this.destino = destino;
        this.tipodevuelo = tipodevuelo;
        this.fechasalida = fechasalida;
        this.fecharegreso = fecharegreso;
    }

    public int getNrodeVuelo() {
        return nrodeVuelo;
    }

    public void setNrodeVuelo(int nrodeVuelo) {
        this.nrodeVuelo = nrodeVuelo;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getTipodevuelo() {
        return tipodevuelo;
    }

    public void setTipodevuelo(String tipodevuelo) {
        this.tipodevuelo = tipodevuelo;
    }

    public Date getFechasalida() {
        return fechasalida;
    }

    public void setFechasalida(Date fechasalida) {
        this.fechasalida = fechasalida;
    }

    public Date getFecharegreso() {
        return fecharegreso;
    }

    public void setFecharegreso(Date fecharegreso) {
        this.fecharegreso = fecharegreso;
    }
}
