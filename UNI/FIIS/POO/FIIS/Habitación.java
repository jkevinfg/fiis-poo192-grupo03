package UNI.FIIS.POO.FIIS;

public class Habitación {
    private String tipodehabitacion;
    private int nrodehabitacion;
    private String caracteristicas;
    private int precio;

    public String getTipodehabitacion() {
        return tipodehabitacion;
    }

    public void setTipodehabitacion(String tipodehabitacion) {
        this.tipodehabitacion = tipodehabitacion;
    }

    public int getNrodehabitacion() {
        return nrodehabitacion;
    }

    public void setNrodehabitacion(int nrodehabitacion) {
        this.nrodehabitacion = nrodehabitacion;
    }

    public String getCaracteristicas() {
        return caracteristicas;
    }

    public void setCaracteristicas(String caracteristicas) {
        this.caracteristicas = caracteristicas;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public int getNrodepiso() {
        return nrodepiso;
    }

    public void setNrodepiso(int nrodepiso) {
        this.nrodepiso = nrodepiso;
    }

    private int nrodepiso;

    public Habitación(String tipodehabitacion, int nrodehabitacion, String caracteristicas, int precio, int nrodepiso) {
        this.tipodehabitacion = tipodehabitacion;
        this.nrodehabitacion = nrodehabitacion;
        this.caracteristicas = caracteristicas;
        this.precio = precio;
        this.nrodepiso = nrodepiso;
    }
}
