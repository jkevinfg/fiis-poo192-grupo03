package UNI.FIIS.POO.FIIS;

import java.util.Date;

public class Tour {
    private int Nrodepersonas;
    private String categoria;
    private Boolean Serviciocomida;
    private Date Fecha;

    public Tour(int nrodepersonas, String categoria, Boolean serviciocomida, Date fecha) {
        Nrodepersonas = nrodepersonas;
        this.categoria = categoria;
        Serviciocomida = serviciocomida;
        Fecha = fecha;
    }

    public int getNrodepersonas() {
        return Nrodepersonas;
    }

    public void setNrodepersonas(int nrodepersonas) {
        Nrodepersonas = nrodepersonas;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public Boolean getServiciocomida() {
        return Serviciocomida;
    }

    public void setServiciocomida(Boolean serviciocomida) {
        Serviciocomida = serviciocomida;
    }

    public Date getFecha() {
        return Fecha;
    }

    public void setFecha(Date fecha) {
        Fecha = fecha;
    }
}
