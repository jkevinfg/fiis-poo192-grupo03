package UNI.FIIS.POO.FIIS;

import java.util.Date;

public class Hotel {
    public String  nombre;
    public Date fechadeingreso;

    public Hotel(String nombre, Date fechadeingreso) {
        this.nombre = nombre;
        this.fechadeingreso = fechadeingreso;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getFechadeingreso() {
        return fechadeingreso;
    }

    public void setFechadeingreso(Date fechadeingreso) {
        this.fechadeingreso = fechadeingreso;
    }
}
