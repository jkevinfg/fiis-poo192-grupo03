package UNI.FIIS.POO.FIIS;

public class Usuario {
    public String nombre;
    private String dni;
    private int contraseña;

    public Usuario(String nombre, String dni, int contraseña) {
        this.nombre = nombre;
        this.dni = dni;
        this.contraseña = contraseña;
    }


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public int getContraseña() {
        return contraseña;
    }

    public void setContraseña(int contraseña) {
        this.contraseña = contraseña;
    }

    
}


