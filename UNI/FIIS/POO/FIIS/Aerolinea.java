package UNI.FIIS.POO.FIIS;

public class Aerolinea {
    private String codigodeavion;
    private int numerodeasiento;
    private String compartimientos;
    private int precio;

    public String getCodigodeavion() {
        return codigodeavion;
    }

    public void setCodigodeavion(String codigodeavion) {
        this.codigodeavion = codigodeavion;
    }

    public int getNumerodeasiento() {
        return numerodeasiento;
    }

    public void setNumerodeasiento(int numerodeasiento) {
        this.numerodeasiento = numerodeasiento;
    }

    public String getCompartimientos() {
        return compartimientos;
    }

    public void setCompartimientos(String compartimientos) {
        this.compartimientos = compartimientos;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public Aerolinea(String codigodeavion, int numerodeasiento, String compartimientos, int precio) {
        this.codigodeavion = codigodeavion;
        this.numerodeasiento = numerodeasiento;
        this.compartimientos = compartimientos;
        this.precio = precio;
    }
}
